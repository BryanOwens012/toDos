# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

(1..2).each do |i|
	Item.create!(user_id: 1, name: "Example #{i}", description: "Sample description #{i}")
end

(1..2).each do |i|
	CompletedItem.create!(user_id: 1, name: "Completed Example #{i}", description: "Sample description #{i}")
end

(1..2).each do |i|
	User.create!(username: "Sample user #{i}")
end
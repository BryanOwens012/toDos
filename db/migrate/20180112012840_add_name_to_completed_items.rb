class AddNameToCompletedItems < ActiveRecord::Migration[5.1]
  def change
    add_column :completed_items, :name, :string
  end
end

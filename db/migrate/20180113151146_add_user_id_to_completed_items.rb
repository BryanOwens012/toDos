class AddUserIdToCompletedItems < ActiveRecord::Migration[5.1]
  def change
    add_column :completed_items, :user_id, :integer
  end
end

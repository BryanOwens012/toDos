class CreateCompletedItems < ActiveRecord::Migration[5.1]
  def change
    create_table :completed_items do |t|

      t.timestamps
    end
  end
end

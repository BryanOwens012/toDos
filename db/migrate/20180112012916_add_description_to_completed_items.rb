class AddDescriptionToCompletedItems < ActiveRecord::Migration[5.1]
  def change
    add_column :completed_items, :description, :text
  end
end

class Api::V2::CompletedItemsController < Api::V2::BaseController
  def index
  	respond_with CompletedItem.all
  end

  def create
  	respond_with :api, :v2, CompletedItem.create(item_params)
  end

  def destroy
  	respond_with CompletedItem.destroy(params[:id])
  end

  def update
  	completedItem = CompletedItem.find(params["id"])
  	completedItem.update_attributes(item_params)
  	respond_with completedItem, json: completedItem
  end

  private

  def item_params
  	params.require(:completedItem).permit(:id, :name, :description, :user_id)
  end

end

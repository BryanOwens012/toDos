class Item < ApplicationRecord
	# http://guides.rubyonrails.org/association_basics.html#belongs-to-association-reference
	belongs_to :user, :foreign_key => 'user_id'
	# Reject user's attempt to edit a task that results in an invalid field
	validates :name, presence: true, length: { minimum: 1, maximum: 100 }
	validates :description, presence: true, length: { minimum: 1, maximum: 1000 }
end

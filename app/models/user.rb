class User < ApplicationRecord
	has_many :items
	has_many :completed_items
	validates :username, presence: true, length: { minimum: 1, maximum: 100 }
end

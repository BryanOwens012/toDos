var AllCompletedItems = createReactClass({

  handlePermDelete(id) {
    this.props.handlePermDelete(id);
    console.log("Permanenly deleted item clicked");
  },

  render: function() {
	var completedItems = 
		(this.props.items == ([] || null))
		? 	"(Empty)"
	    :	this.props.items.map((item) => {
		    	return item.user_id != this.props.user_id
		    		? 	null
		    		: 	<div key={item.id}>
			           		<CompletedItem item={item}
			                	handlePermDelete={this.handlePermDelete.bind(this, item)} />
		    			</div>
			});

    return (
    	<div className='completed'>
    		<h2>Completed tasks:</h2>
			{completedItems}
		</div>
    )
  }
});


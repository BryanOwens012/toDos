var AllItems = createReactClass({

  handleDelete(id) {
    this.props.handleDelete(id);
    console.log("Delete item clicked");
  },

  onUpdate(item) {
    this.props.onUpdate(item);
  },


  render: function() {
  	var items = this.props.items.map((item) => {
      return item.user_id != this.props.user_id
        ?   null
    		:   <div key={item.id}>
              <Item item={item}
                  user_id={this.props.user_id}
                  handleDelete={this.handleDelete.bind(this, item)}
                  handleUpdate={this.onUpdate} />
      			</div>
    });

    return (
    	<div>
			   {items}
		  </div>
    )
  }
});


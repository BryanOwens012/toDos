var Body = createReactClass({

 	
	getInitialState() {
		return { 
			items: [],
			completedItems: [],
			users: [],
			hideCompletedItems: false,
			user_id: 1
		}
		console.log("Got initial state");
	},

	componentDidMount() {
		console.log("component <Body /> has mounted");
		$.getJSON('/api/v1/items.json',
			(response) => {
				this.setState({ items: response });
				console.log("Received items.json: ", this.state.items);
				$.getJSON('/api/v2/completed_items.json',
					(response) => {
						this.setState({ completedItems: response});
						console.log("Received completed_items.json: ", this.state.completedItems);
						$.getJSON('/api/v3/users.json',
							(response) => {
								this.setState({ users: response });
								console.log("Received users.json: ", this.state.users);
							}
						);
				});
			}
		);
	},

	handleDelete(item) {
		var item_id = item.id;
		$.ajax({
			url: '/api/v2/completed_items/',
			type: 'POST',
			data: { completedItem: { user_id: this.state.user_id, id: item_id, name: item.name, description: item.description } },
			success: (item) => {
				this.setState({ completedItems: this.state.completedItems.concat(item) });
				console.log(item , " has been moved to the completedItems db");
				$.ajax({
					url: '/api/v1/items/' + item_id,
					type: 'DELETE',
					success: (item) => {
						this.removeItemClient(item_id);
						console.log('successfully removed item');
					}
				});
			}
		});
		
	},

	handlePermDelete(item) {
		var item_id = item.id;
		$.ajax({
			url: '/api/v2/completed_items/' + item_id,
			type: 'DELETE',
			success: (item) => {
				this.permRemoveItemClient(item_id);
				console.log('successfully removed item');
			}
		});		
	},

	 handleUpdate(item) {
	    $.ajax({
	        url: '/api/v1/items/' + item.id,
	        type: 'PUT',
	        data: {item: item},
	        success: () => {
	        	this.updateItems(item);
	          console.log('successfully updated item!');
	        }

	    });
	 },

	 handleDeleteHover(id) {
	 	handleDelete(id);
	 },


	 updateItems(item) {
	 	var items = this.state.items.map( (i) => {
	 		if (i.id == item.id) {
	 			return item;
	 		}
	 		else {
	 			return i;
	 		}
	 	});
	 	this.setState({items: items});
	 },

	 /* // For making the newest edited tasks always go to the end:
	 updateItems(item) {
	 	var items = this.state.items.filter( (i) => {
	 		return i.id != item.id
	 	});
	 	items.push(item);
	 	this.setState({items: items});
	 },
	 */


	removeItemClient(id) {
		newItems = this.state.items.filter( (item) => {
			return item.id != id;
		});
		this.setState({ items: newItems });
	},

	permRemoveItemClient(id) {
		newItems = this.state.completedItems.filter( (item) => {
			return item.id != id;
		});
		this.setState({ completedItems: newItems });
	},

	handleSubmit(item) {
		var newState = this.state.items.concat(item);
		this.setState({ items: newState })
	},

	
	handleCheckboxChange(checkbox) {
		this.setState({ hideCompletedItems: !this.state.hideCompletedItems });
		console.log("Toggle hideCompletedItems to: ", this.state.hideCompletedItems);
	},

	handleSelectChange(event) {
		this.setState({ user_id: event.target.value });
	},


	/*
	setUserId(id) {
		this.setState({ user_id: id });
		this.setState({ itemsWithId: 
			this.state.items.filter(
				(item) => {
					return (item.user_id == id);
				}
			)
		});
		this.setState({ completedItemsWithId: 
			this.state.completedItems.filter(
				(item) => {
					return (item.user_id == id);
				}
			)
		});
		console.log("user_id has been set to: ", this.state.user_id);
		console.log("itemsWithId has been set to: ", this.state.itemsWithId);
		console.log("completedItemsWithId has been set to: ", this.state.completedItemsWithId);
	},
	*/
	

  render: function() {
  	var options = this.state.users.map((user) => {
  		return (
  			<option key={user.id} value={user.id}>
  				#{user.id} ({user.username})
  			</option>
  		)
  	});
    return (
    	<div>
    		<NewItem user_id={this.state.user_id} handleSubmit={this.handleSubmit} />
    		<input type='checkbox' onClick={this.handleCheckboxChange} /> &nbsp; Hide completed tasks
    		<br />
    		Switch to user: &nbsp;
    		<select onChange={this.handleSelectChange}>
    			{options}
    		</select>
    		<br />
    		<br />
    		<hr />
    		<br />
    		<h2>Current tasks:
    		</h2>
    		<h3>(Newest tasks on bottom, click tasks to delete them without using the "delete" button)</h3>
    		<SeedTasks user_id={this.state.user_id} handleSubmit={this.handleSubmit} />
    		<br />
    		<AllItems user_id={this.state.user_id} items={this.state.items} handleDelete={this.handleDelete} onUpdate={this.handleUpdate} />
    		<hr />
    		{this.state.hideCompletedItems
    			? 	null
    			: 	<AllCompletedItems user_id={this.state.user_id} items={this.state.completedItems} handlePermDelete={this.handlePermDelete} />
    		}
    		
    	</div>
    );
  }
});


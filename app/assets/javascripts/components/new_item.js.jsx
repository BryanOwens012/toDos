var NewItem = createReactClass({

	getInitialState() {
		return {
			validateFlag: -1, // -1 == no message, false == validation failed, true == validation succeeded
			name: "",
			description: ""
		}
	},

	validateName(name) {
		var REG = /\S+/;
		return (REG.test(name) && name.length <= 100); // name max length as defined in Item model
	},

	validateDescription(description) {
		var REG = /\S+/;
		return (REG.test(description) && description.length <= 1000); // description max length as defined in Item model
	},

	validateAll() {
		this.setState({ validateFlag: (this.validateName(this.state.name) && this.validateDescription(this.state.description)) });
		console.log("this.state.validateFlag == " , this.state.validateFlag, " when name == ", this.state.name, " and description = ", this.state.description);
	},

	handleClick() {
		var name = this.refs.name.value;
		var description = this.refs.description.value;
		this.state.validateFlag
			?	$.ajax({
					url: '/api/v1/items',
					type: 'POST',
					data: { item: { user_id: this.props.user_id, name: name, description: description } },
					success: (item) => {
						this.props.handleSubmit(item);
					}
				})
			: null;
	},

	handleChangeName(event) {
		var value = event.target.value;
		this.setState({
				name: value,
		});
		this.validateAll();
	},

	handleChangeDescription(event) {
		var value = event.target.value;
		this.setState({
				description: value,
		});
		this.validateAll(); 
	},


  render: function() {
    return (
    	<div>
    		<input ref='name' placeholder='Enter task name' onChange={this.handleChangeName}/>
    		<input ref='description' placeholder='Enter task description' onChange={this.handleChangeDescription} />
    			<button onClick={this.handleClick} disabled={!this.state.validateFlag}>Submit</button>
			<ValidationMsg validationResult={this.state.validateFlag} />
    	</div>
    );
  }
});


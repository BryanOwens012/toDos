var Main = createReactClass({

  render: function() {
    return (
    	<div>
    		<Header />
    		<Body />
    		<Footer />
    	</div>
    )
  }
});
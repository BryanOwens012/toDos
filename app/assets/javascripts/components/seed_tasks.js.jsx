var SeedTasks = createReactClass({

	handleClick() {
		$.ajax({
			url: '/api/v1/items',
			type: 'POST',
			data: { item: { user_id: this.props.user_id, name: "Generated task", description: "Generated description" }},
			// data: { item: { name: "Generated task #", description: "Generated description #"}},
			success: (item) => {
				this.props.handleSubmit(item);
			}
		});
	},

  render: function() {
    return (
    	<button onClick={this.handleClick}>
    		Generate a task
    	</button>
    );
  }
});


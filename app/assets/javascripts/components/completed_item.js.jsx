var CompletedItem = createReactClass({

	getInitialState() {
		return {
			name: '',
			description: ''
		}
	},

	componentDidMount() {
		this.setState( {
			name: this.props.item.name,
			description: this.props.item.description
		});
	},

  render: function() {
    	return (
	    	<div>
	    		<table>
	    			<tbody>
		    			<tr>
							<td>
								<h4>{this.state.name}</h4>
							</td>

							<td>{this.state.description}</td>
							<td>
				          		<button className='delete' onClick={this.props.handlePermDelete}>Permanently delete</button>
				          	</td>
					     </tr>
					</tbody>
		         </table>
	         </div>
         )
 	}
});


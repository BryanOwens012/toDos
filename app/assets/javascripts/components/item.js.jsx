var Item = createReactClass({

	getInitialState() {
		return {
			editable: false,
			name: '',
			description: ''
		}
	},


	handleEdit() {
		if (this.state.editable) {
			var name = this.refs.name.value;
			var description = this.refs.description.value;
			var id = this.props.item.id;
			var item = {user_id: this.props.user_id, id: id, name: name, description: description};
			this.props.handleUpdate(item);
			console.log('in handleEdit', this.state.editable, name, description);
		}
		this.setState({ editable: !this.state.editable})
  	},

  render: function() {
  		var nameDescription = this.state.editable
  			? 	<span>
  					<td>
	  					<input type='text' ref='name' defaultValue={this.props.item.name} />
	  				</td>
	  				<td>
	  					<input type='text' ref='description' defaultValue={this.props.item.description} />
	  				</td>
  				</span>
  			: 	<span className='hover-delete' onClick={this.props.handleDelete}>
  					<td>
	  					<h4>
	  						{this.props.item.name}
	  					</h4>
	  				</td>
	  				<td>{this.props.item.description}</td>
  				</span>;
  				
    	return (
	    	<div>
	    		<table>
	    			<tbody>
		    			<tr>
		    				
		    			{nameDescription}
							
							<td>
				          		<button className='delete' onClick={this.props.handleDelete}>Delete</button>
				          	</td>
				          	<td>
					         	<button className='edit' onClick={this.handleEdit}>
					         		{this.state.editable
					         			? "Done"
					         			: "Edit"
					         		}
					         	</button>
					         </td>
					     </tr>
					</tbody>
		         </table>
	         </div>
         )
 	}
});


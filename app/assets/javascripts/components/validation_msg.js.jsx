var ValidationMsg = createReactClass({


	/*
	getInitialState() {
		return {
			validateFlag: -1
		}
	},

	componentDidMount() {
		this.setState( {validateFlag: this.props.validationResult} );
		console.log("validateFlag == ", this.state.validateFlag);
	},
	*/

  render: function() {
  	var msg;
  	if (this.props.validationResult == -1) {
  		msg = () => {
  			return (
  				<div className='validationHello'> To see validation, type in words for a new task above. </div>
  			);
  		}
  	}
  	else if (this.props.validationResult == true) {
  		msg = () => {
  			return (
  				<div className='validationSuccess'> Validation success. </div>
  			);
  		}
  	}
  	else if (this.props.validationResult == false) {
  		msg = () => {
  			return (
  				<div className='validationError'> Validation error! Your name or description is either empty or too long. Try again.</div>
  			);
  		}
  	}
    return (
    	<div className='validationMsg'>
    		{msg()}
    	</div>
    );
  }
});


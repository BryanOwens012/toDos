Rails.application.routes.draw do
  

  # namespace :api do
  #   namespace :v3 do
  #     get 'users/index'
  #   end
  # end

  # namespace :api do
  #   namespace :v3 do
  #     get 'users/create'
  #   end
  # end

  # namespace :api do
  #   namespace :v3 do
  #     get 'users/destroy'
  #   end
  # end

  # namespace :api do
  #   namespace :v3 do
  #     get 'users/update'
  #   end
  # end

  # namespace :api do
  #   namespace :v2 do
  #     get 'completed_items/index'
  #   end
  # end

  # namespace :api do
  #   namespace :v2 do
  #     get 'completed_items/create'
  #   end
  # end

  # namespace :api do
  #   namespace :v2 do
  #     get 'completed_items/destroy'
  #   end
  # end

  # namespace :api do
  #   namespace :v2 do
  #     get 'completed_items/update'
  #   end
  # end


  get 'site/index'

  root to: 'site#index'

  ## open tasks:

  namespace :api do
    namespace :v1 do
      get 'base/index'
    end
  end

  namespace :api do
    namespace :v1 do
      get 'items/index'
    end
  end

  namespace :api do
    namespace :v1 do
      resources :items, only: [:index, :create, :destroy, :update]      
    end    
  end

  ## completed tasks:

   namespace :api do
    namespace :v2 do
      get 'base/index'
    end
  end

  namespace :api do
    namespace :v2 do
      get 'completed_items/index'
    end
  end

  namespace :api do
    namespace :v2 do
      resources :completed_items, only: [:index, :create, :destroy, :update]
    end
  end

  ## users db:

  namespace :api do
    namespace :v3 do
      get 'base/index'
    end
  end

  namespace :api do
      namespace :v3 do
        get 'users/index'
      end
  end

  namespace :api do
    namespace :v3 do
      resources :users, only: [:index, :create, :destroy, :update]
    end
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

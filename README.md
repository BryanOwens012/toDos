# Welcome to my ToDos project!

This React-Rails project is written in React version ES5. The React front-end uses ajax/json to modify the sqlite database maintained by the Rails back-end. Changes to the database are persistent. There are a bunch of stateful features, as well as 3 database-backed models, two of which are each associated with the third. The model files include some extra code besides just for association.

I began by writing it using webpacker and React ES6 (ES6 puts JS components inside `app/components`, declares classes with the syntax `class App extends React.Component`, etc.) However, I ran into problems integrating Rails with React and had to spend an enormous number of hours debugging.

So I scrapped [the original version](https://gitlab.com/BryanOwens012/toDo-app) and looked up [a tutorial](https://www.pluralsight.com/guides/ruby-ruby-on-rails/building-a-crud-interface-with-react-and-ruby-on-rails) for a generic React-Rails CRUD interface, which was written in React ES5 (ES5 puts JS components inside `app/assets/javascripts/components`, declares classes with the syntax `var App = createReactClass({})`, etc.) I ended up continuing with ES5 until the end.

I did not use `rails generate scaffold`.

## React stateful features:
- Create/edit/delete tasks
- Button to generate a sample task
- Dynamic validation during task creation
- Tasks can be deleted by simply clicking on them
- Checkbox to toggle hiding of the completed tasks list
- Select box to switch between users, to display their respective tasks

## Rails features:
- Open tasks (`items_controller.rb`, `item.rb`)
- Completed tasks (`completed_items_controller.rb`, `completed_item.rb`)
- Users (`user_controller.rb`, `user.rb`). `User` `has_many` open tasks and completed tasks (association).
- Validation for all 3 models, so that the user's edit is rejected if it would make a field empty or too long (either 100 chars or 1000 chars, depending on the field).

## Here's what it looks like (image):

![ToDos project](./screenshots/sample_screen.png)
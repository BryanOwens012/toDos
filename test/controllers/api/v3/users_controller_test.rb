require 'test_helper'

class Api::V3::UsersControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get api_v3_users_index_url
    assert_response :success
  end

  test "should get create" do
    get api_v3_users_create_url
    assert_response :success
  end

  test "should get destroy" do
    get api_v3_users_destroy_url
    assert_response :success
  end

  test "should get update" do
    get api_v3_users_update_url
    assert_response :success
  end

end
